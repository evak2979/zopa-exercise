While building the TinyCsvWrapper + Tests:
I have purposefully read the csv filename from the running assembly directory - in an ideal situation, I would have created an appropriate wrapper to get the path and mock/test it. However, since the particular exercise's purpose is to test the loading of the file + the algorithm, I left it as it was. I performed testing in integration vs a valid and an invalid file to make up for the lack of a proper path detector interface

*Testing*

I have unit tested every file and added integration tests around the reading of CSVs. But I have not added unit testing on the Console UI. I have assumed legal inputs, so as to focus on the solution at hand rather than making sure the user always passes in legit parameter values (in my experience, past interviews did not care for this bit so I assumed this to be the case). If, however, you wish me to also implement an input parser for console read lines, let me know!

*Code Comments!*

I do -not- comment my code, as per the good practices standard (code should be self explanatory). However, I added a couple of comments to help with my reasoning behind some design ideas as I was coding. This is merely to provide the reviewer with my train of thought as I was coding, something that normally would happen during the pair programming phase. Thanks !


*Integration tests + Manual testing*
In order to run integration tests, please copy the invalidcsvfile.csv, and the validcsvfile.csv from the the solution's root, to the debug/bin directory of the ZopaExercise.Integration project once the solution
has been built. I realized the .gitignore file had removed them from the commit only after I had pushed the final solution. Both files can be found in the project's root. Equally, in order to test the
application one should paste a .csv file in the same directoryas the .executable file of the ConsoleUI. Thanks!