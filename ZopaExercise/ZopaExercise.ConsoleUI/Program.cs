﻿using System;
using System.Text;
using SystemWrapper.IO;
using ZopaExercise.Business.Concretes;
using ZopaExercise.Business.Interfaces;
using ZopaExercise.Business.Models;
using ZopaExercise.Domain.CSV;
using ZopaExercise.Domain.DTOs;

namespace ZopaExercise.ConsoleUI
{
    public class Program
    {
        static void Main(string[] args)
        {
            if(args.Length < 2 || args.Length > 2)
                throw new Exception("Invalid number of arguments. Please input filename as first argument, requested amount" +
                                    " divided by 100, and within 100 and 15000 for second argument");

            InputValidator validator = new InputValidator(1000,15000);

            if (!string.IsNullOrEmpty(validator.IsValidAmount(int.Parse(args[1]))))
            {
                throw new Exception(validator.IsValidAmount(int.Parse(args[1])));
            }

            //I would have used DI here, but I went with the direct approach of instantinating for the sake
            //of the exercise. if you wish me to include DI, please let me know.
            ILoanDirector director = new LoanDirector(new LoanBuilder(new TinyCsvParserWrapper(new FileWrap(), 
                new CsvFileOptions(args[0], Encoding.ASCII, new [] {','}))
                , new LoanFactory(new CompoundInterestRepayCalculator()),
                new LoanOptions()
                {
                    RequestedAmount = int.Parse(args[1]),
                    DurationOfPaymentInMonths = 36
                }));

            LoanDto loanDto = director.Construct();
            DisplayResult(loanDto);
            Console.ReadLine();
        }

        private static void DisplayResult(LoanDto loanDto)
        {
            Console.WriteLine($"Requested Amount: £{loanDto.RequestedAmount}");
            Console.WriteLine($"Rate: {loanDto.Rate}%");
            Console.WriteLine($"Monthly Repayment: £{loanDto.MonthlyRepayment}");
            Console.WriteLine($"Total Repayment: £{loanDto.TotalRepayment}");
            Console.ReadLine();
        }
    }
}
