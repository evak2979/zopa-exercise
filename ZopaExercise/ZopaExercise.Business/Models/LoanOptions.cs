﻿using System.Collections.Generic;
using ZopaExercise.Domain.DTOs;

namespace ZopaExercise.Business.Models
{
    public class LoanOptions
    {
        public int DurationOfPaymentInMonths { get; set; }
        public int RequestedAmount { get; set; }
        public IEnumerable<LenderDto> Lenders { get; set; }
    }
}
