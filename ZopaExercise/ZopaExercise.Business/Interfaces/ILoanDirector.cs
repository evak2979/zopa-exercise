﻿using ZopaExercise.Domain.DTOs;

namespace ZopaExercise.Business.Interfaces
{
    public interface ILoanDirector
    {
        LoanDto Construct();
    }
}
