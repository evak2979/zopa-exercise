﻿using ZopaExercise.Domain.DTOs;

namespace ZopaExercise.Business.Interfaces
{
    public interface ILoanBuilder
    {
        void InitializeLenders();
        LoanDto Build();
    }
}
