﻿namespace ZopaExercise.Business.Interfaces
{
    public interface IInputValidator
    {
        string IsValidAmount(int amount);
    }
}
