﻿using System.Collections.Generic;

namespace ZopaExercise.Business.Interfaces
{
    public interface ILendersRepository<T>
    {
        IEnumerable<T> Get();
    }
}
