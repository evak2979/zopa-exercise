﻿using ZopaExercise.Domain.DTOs;

namespace ZopaExercise.Business.Interfaces
{
    public interface IRepayCalculator
    {
        double GetTotalRepay(LoanDto loanDto);
        double GetMonthlyPayment(LoanDto loanDto);
        double GetRate(LoanDto loanDto);
    }
}
