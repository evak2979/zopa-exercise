﻿using ZopaExercise.Business.Models;
using ZopaExercise.Domain.DTOs;

namespace ZopaExercise.Business.Interfaces
{
    public interface ILoanFactory
    {
        LoanDto Create(LoanOptions loanOptions);
    }
}
