﻿using ZopaExercise.Business.Interfaces;

namespace ZopaExercise.Business.Concretes
{
    public class InputValidator : IInputValidator
    {
        private readonly int _minAmount;
        private readonly int _maxAmount;

        public InputValidator(int minAmount, int maxAmount)
        {
            _minAmount = minAmount;
            _maxAmount = maxAmount;
        }

        public string IsValidAmount(int amount)
        {
            if(!IsWithinAProperRange(amount))
                return "Requested amount must be between 100 and 15000";

            if (!IsDivisibleByOneHundred(amount))
                return "Requested amount must be directly divisible by 100";

            return string.Empty;
        }

        private bool IsDivisibleByOneHundred(int amount)
        {
            return amount % 100 == 0;
        }

        private bool IsWithinAProperRange(int amount)
        {
            return (amount >= _minAmount && amount <= _maxAmount);
        }
    }
}
