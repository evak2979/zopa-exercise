﻿using System;
using System.Collections.Generic;
using ZopaExercise.Business.Interfaces;
using ZopaExercise.Business.Models;
using ZopaExercise.Domain.DTOs;

namespace ZopaExercise.Business.Concretes
{
    public class LoanBuilder : ILoanBuilder
    {
        private readonly ILendersRepository<LenderDto> _lendersRepository;
        private readonly ILoanFactory _loanFactory;
        private readonly LoanOptions _loanOptions;
        private IEnumerable<LenderDto> _lenders;

        public LoanBuilder(ILendersRepository<LenderDto> lendersRepository, 
            ILoanFactory loanFactory, LoanOptions loanOptions)
        {
            _lendersRepository = lendersRepository;
            _loanFactory = loanFactory;
            _loanOptions = loanOptions;
        }

        public void InitializeLenders()
        {
            _lenders = _lendersRepository.Get();
        }

        //For this particular variation of Builder, InitializeLenders needs to be called in
        //advance by the director. In future variations (if any), this may be unecessary.
        public LoanDto Build()
        {
            if(_lenders == null)
                throw new Exception("You must call InitializeLenders before calling Build().");

            _loanOptions.Lenders = _lenders;
            return _loanFactory.Create(_loanOptions);
        }
    }
}
