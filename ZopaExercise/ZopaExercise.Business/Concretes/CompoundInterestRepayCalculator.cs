﻿using System;
using System.Linq;
using ZopaExercise.Business.Interfaces;
using ZopaExercise.Domain.DTOs;

namespace ZopaExercise.Business.Concretes
{
    //For me to implement the right formula, I used this site: http://www.thecalculatorsite.com/articles/finance/compound-interest-formula.php
    //As well as dotnet perls implementation of it. Through unit testng the values I am getting seem to match
    //with the values from the website for the particular .csv file. That was one of my points of concern, as to whether
    //I understood the formula to be used correctly. In case I have not, and you wish me to implement it in a
    //different approach, let me know if you so wish it. Thank you.
    public class CompoundInterestRepayCalculator : IRepayCalculator
    {
        public double GetTotalRepay(LoanDto loanDto)
        {
            double body = 1 + (loanDto.Rate / 100 / 12);
            double exponent = 12 * loanDto.DurationOfPaymentInMonths / 12;
            return Math.Round(loanDto.RequestedAmount * Math.Pow(body, exponent), 2);
        }

        public double GetMonthlyPayment(LoanDto loanDto)
        {
            return Math.Round(loanDto.TotalRepayment / loanDto.DurationOfPaymentInMonths, 2);
        }

        public double GetRate(LoanDto loanDto)
        {
            return Math.Round(loanDto.LendersToOfferLoan.Sum(x => x.Rate) / loanDto.LendersToOfferLoan.Count() * 100, 2);
        }
    }
}
