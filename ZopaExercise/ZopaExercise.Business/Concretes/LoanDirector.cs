﻿using ZopaExercise.Business.Interfaces;
using ZopaExercise.Domain.DTOs;

namespace ZopaExercise.Business.Concretes
{
    public class LoanDirector : ILoanDirector
    {
        private readonly ILoanBuilder _builder;

        public LoanDirector(ILoanBuilder builder)
        {
            _builder = builder;
        }

        public LoanDto Construct()
        {
            _builder.InitializeLenders();
            return _builder.Build();
        }
    }
}
