﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Reflection;
using SystemWrapper.IO;
using TinyCsvParser;
using TinyCsvParser.Mapping;
using ZopaExercise.Business.Interfaces;
using ZopaExercise.Domain.CSV;
using ZopaExercise.Domain.DTOs;

namespace ZopaExercise.Business.Concretes
{
    [ExcludeFromCodeCoverage]
    public class TinyCsvParserWrapper : ILendersRepository<LenderDto>
    {
        private readonly IFileWrap _fileWrap;
        private readonly CsvFileOptions _options;

        public TinyCsvParserWrapper(IFileWrap fileWrap, CsvFileOptions options)
        {
            _fileWrap = fileWrap;
            _options = options;
        }

        public IEnumerable<LenderDto> Get()
        {
            _options.FileName = GetFullFileName();
            if(!_fileWrap.Exists(_options.FileName))
                throw new FileNotFoundException();

            CsvLenderMapping csvMapper = new CsvLenderMapping();
            CsvParserOptions csvParserOptions = GetTinyCsvOptions();
            CsvParser<LenderDto> csvParser = GetCsvParserForLenderDto(csvParserOptions, csvMapper);

            return GetValidLenderDtos(csvParser);
        }

        private string GetFullFileName()
        {
            return Path.Combine(AssemblyDirectory, _options.FileName);
        }

        private IEnumerable<LenderDto> GetValidLenderDtos(CsvParser<LenderDto> csvParser)
        {
            return csvParser
                .ReadFromFile(_options.FileName, _options.Encoding)
                .Where(x => x.IsValid)
                .Select(x => x.Result)
                .ToList();
        }

        private CsvParser<LenderDto> GetCsvParserForLenderDto(CsvParserOptions csvParserOptions, CsvLenderMapping csvMapper)
        {
            return new CsvParser<LenderDto>(csvParserOptions, csvMapper);
        }

        private CsvParserOptions GetTinyCsvOptions()
        {
            CsvParserOptions csvParserOptions = new CsvParserOptions(_options.SkipHeader, _options.FieldsSeparator);
            return csvParserOptions;
        }

        private string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        private class CsvLenderMapping : CsvMapping<LenderDto>
        {
            public CsvLenderMapping() : base()
            {
                MapProperty(0, x => x.Name);
                MapProperty(1, x => x.Rate);
                MapProperty(2, x => x.Available);
            }
        }
    }
}
