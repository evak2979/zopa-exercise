﻿using System.Collections.Generic;
using System.Linq;
using ZopaExercise.Business.Interfaces;
using ZopaExercise.Business.Models;
using ZopaExercise.Domain.DTOs;

namespace ZopaExercise.Business.Concretes
{
    public class LoanFactory : ILoanFactory
    {
        private readonly IRepayCalculator _repayCalculator;
        private IEnumerable<LenderDto> _lenders;
        private LoanDto _loanDto;

        public LoanFactory(IRepayCalculator repayCalculator)
        {
            _repayCalculator = repayCalculator;
        }

        //Though this is called as a factory, the repayCalculator methods could
        //have been wrapped in a Builder pattern, with a Director constructing around it.
        //I came to realize this way too far down in the development process and did not
        //wish to overengineer the solution by this point!
        public LoanDto Create(LoanOptions loanOptions)
        {
            _lenders = loanOptions.Lenders;

            InitializeLoanPrimaryValues(loanOptions);
            DetermineIfLoanAmountIsAvailable();
            if (!_loanDto.IsAvailable)
                return _loanDto;

            OrderLendersByLowestRate();
            InitializeTheLendersWhoWillProvideTheLoan();

            _loanDto.Rate = _repayCalculator.GetRate(_loanDto);
            _loanDto.TotalRepayment = _repayCalculator.GetTotalRepay(_loanDto);
            _loanDto.MonthlyRepayment = _repayCalculator.GetMonthlyPayment(_loanDto);

            return _loanDto;
        }

        private void DetermineIfLoanAmountIsAvailable()
        {
            _loanDto.IsAvailable = _lenders.Sum(x => x.Available) >= _loanDto.RequestedAmount;
        }

        private void InitializeLoanPrimaryValues(LoanOptions loanOptions)
        {
            _loanDto = new LoanDto
            {
                DurationOfPaymentInMonths = loanOptions.DurationOfPaymentInMonths,
                RequestedAmount = loanOptions.RequestedAmount
            };
        }

        private void OrderLendersByLowestRate()
        {
            _lenders = _lenders.OrderBy(x => x.Rate);
        }

        private void InitializeTheLendersWhoWillProvideTheLoan()
        {
            double lenderAmount = 0;
            List<LenderDto> lendersToGiveLoan = new List<LenderDto>();

            foreach (LenderDto lender in _lenders)
            {
                lenderAmount += lender.Available;
                lendersToGiveLoan.Add(lender);

                if (lenderAmount >= _loanDto.RequestedAmount)
                    break;
            }

            _loanDto.LendersToOfferLoan = lendersToGiveLoan;
        }
    }
}
