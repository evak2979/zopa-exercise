﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using SystemWrapper.IO;
using NUnit.Framework;
using ZopaExercise.Business.Concretes;
using ZopaExercise.Domain.CSV;
using ZopaExercise.Domain.DTOs;

namespace ZopaExercise.Integration
{
    [TestFixture]
    public class TinyCsvParserWrapperTests
    {
        private TinyCsvParserWrapper _sut;

        [Test]
        public void should_return_empty_list_of_lenderdtos_for_invalid_csv_file()
        {
            //given
            _sut = new TinyCsvParserWrapper(new FileWrap(), new CsvFileOptions("invalidcsvfile.csv", Encoding.ASCII, new[] { ',' }));

            //when
            IEnumerable<LenderDto> result =  _sut.Get();

            //then
            Assert.IsFalse(result.Any());
        }

        [Test]
        public void should_return_populated_list_of_lenderdtos_for_valid_csv_file()
        {
            //given
            _sut = new TinyCsvParserWrapper(new FileWrap(), new CsvFileOptions("validcsvfile.csv", Encoding.ASCII, new[] { ',' }));

            //when
            IEnumerable<LenderDto> result = _sut.Get();

            //then
            Assert.IsTrue(result.Any());
            Assert.That(result.First().Name, Is.EqualTo("Jane"));
            Assert.That(result.First().Rate, Is.EqualTo((double)0.069));
            Assert.That(result.First().Available, Is.EqualTo(480));
        }
    }
}
