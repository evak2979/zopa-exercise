﻿using System;
using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using ZopaExercise.Business.Concretes;
using ZopaExercise.Business.Interfaces;
using ZopaExercise.Business.Models;
using ZopaExercise.Domain.DTOs;

namespace ZopaExercise.Unit
{
    [TestFixture]
    public class LoanBuilderTests
    {
        private Mock<ILendersRepository<LenderDto>> _mockLendersRepo;
        private Mock<ILoanFactory> _mockLoanFactory;
        private LoanBuilder _sut;
        private LoanOptions _loanOptions;

        [SetUp]
        public void Setup()
        {
            _loanOptions = new LoanOptions();
            _mockLoanFactory = new Mock<ILoanFactory>();
            _mockLendersRepo = new Mock<ILendersRepository<LenderDto>>();
            _sut = new LoanBuilder(_mockLendersRepo.Object, _mockLoanFactory.Object, _loanOptions);
        }

        [Test]
        public void should_call_the_lenders_repo_when_initializing_lenders()
        {
            //when
            _sut.InitializeLenders();

            //then
            _mockLendersRepo.Verify(x=>x.Get());
        }

        [Test]
        public void should_throw_exception_if_calling_build_before_calling_initialize_lenders()
        {
            //given + when + then
            Assert.Throws<Exception>(() => _sut.Build(), "You must call InitializeLenders before calling Build().");
        }

        [Test]
        public void should_call_the_loanfactory_with_the_correct_list_of_lenders_when_calling_build()
        {
            //given
            IEnumerable<LenderDto> someLenders = new List<LenderDto> {new LenderDto(), new LenderDto()};
            _mockLendersRepo.Setup(x => x.Get()).Returns(someLenders);

            //when
            _sut.InitializeLenders();
            _sut.Build();

            //then
            _mockLoanFactory.Verify(x => x.Create(_loanOptions));
        }
    }
}
