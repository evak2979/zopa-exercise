﻿using System.Collections.Generic;
using NUnit.Framework;
using ZopaExercise.Business.Concretes;
using ZopaExercise.Domain.DTOs;

namespace ZopaExercise.Unit
{
    [TestFixture]
    public class CompoundInterestRepayCalculatorTests
    {
        private CompoundInterestRepayCalculator _sut = new CompoundInterestRepayCalculator();
        private LoanDto _loanDto;

        [SetUp]
        public void Setup()
        {
            _loanDto = new LoanDto()
            {
                RequestedAmount = 1000,
                DurationOfPaymentInMonths = 36,
                Rate = 7
            };
        }

        [Test]
        public void should_calculate_the_total_repay_correctly()
        {
            //when
            double totalRepay = _sut.GetTotalRepay(_loanDto);

            //then
            Assert.That(totalRepay, Is.EqualTo(1232.93));
        }

        [Test]
        public void should_calculate_the_monthly_repay_correctly()
        {
            //given 
            _loanDto.TotalRepayment = 1232.93;

            //when
            double monthlyRepay = _sut.GetMonthlyPayment(_loanDto);

            //then
            Assert.That(monthlyRepay, Is.EqualTo(34.25));
        }

        [Test]
        public void should_properly_populate_the_loan_rate_for_a_single_lender()
        {
            //given
            LoanDto loanDto = new LoanDto()
            {
                LendersToOfferLoan = new List<LenderDto>()
                {
                    new LenderDto() {Available = 500, Rate = 0.069}
                }
            };

            //when
            double result = _sut.GetRate(loanDto);

            //then
            Assert.That(result, Is.EqualTo(6.90));
        }

        [Test]
        public void should_properly_populate_the_loan_rate_for_multiple_lenders()
        {
            //given
            LoanDto loanDto = new LoanDto()
            {
                LendersToOfferLoan = new List<LenderDto>()
                {
                    new LenderDto() {Available = 500, Rate = 0.069},
                    new LenderDto() {Available = 500, Rate = 0.071}
                }
            };

            //when
            double result = _sut.GetRate(loanDto);

            //then
            Assert.That(result, Is.EqualTo(7.00));
        }
    }
}
