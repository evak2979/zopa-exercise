﻿using System.Linq;
using Moq;
using NUnit.Framework;
using ZopaExercise.Business.Concretes;
using ZopaExercise.Business.Interfaces;
using ZopaExercise.Business.Models;
using ZopaExercise.Domain.DTOs;

namespace ZopaExercise.Unit
{
    [TestFixture]
    public class LoanStrategyTests
    {
        private Mock<IRepayCalculator> _mockCalculator;
        private LoanFactory _sut;

        [SetUp]
        public void Setup()
        {
            _mockCalculator = new Mock<IRepayCalculator>();
            _sut = new LoanFactory(_mockCalculator.Object);
        }

        private LoanOptions GetLoanOptionsForTesting(int durationOfPaymentInMonths, int requestedAmount,
            params LenderDto[] lenders)
        {
            LoanOptions loanOptions = new LoanOptions();
            loanOptions.DurationOfPaymentInMonths = durationOfPaymentInMonths;
            loanOptions.RequestedAmount = requestedAmount;

            loanOptions.Lenders = lenders;

            return loanOptions;
        }

        [Test]
        public void should_return_a_loandto_with_available_false_if_lenders_available_amount_less_than_requested_for_single_lender()
        {
            //given
            LoanOptions loanOptions = GetLoanOptionsForTesting(36, 1000, new LenderDto() {Available = 999});

            //when
            LoanDto result = _sut.Create(loanOptions);

            //then
            Assert.That(result.IsAvailable, Is.EqualTo(false));
        }

        [Test]
        public void should_return_a_loandto_with_available_false_if_lenders_available_amount_less_than_requested_for_multiple_lenders()
        {
            //given
            LoanOptions loanOptions = GetLoanOptionsForTesting(36, 1000, new LenderDto() { Available = 500 },
                new LenderDto() { Available = 499 });

            //when
            LoanDto result = _sut.Create(loanOptions);

            //then
            Assert.That(result.IsAvailable, Is.EqualTo(false));
        }

        [Test]
        public void should_call_the_mock_repay_calculator_to_calculate_total_repay_for_single_lender()
        {
            //given
            _mockCalculator.Setup(x => x.GetTotalRepay(It.IsAny<LoanDto>())).Returns(100);
            _mockCalculator.Setup(x => x.GetRate(It.IsAny<LoanDto>())).Returns(1);
            LoanOptions loanOptions = GetLoanOptionsForTesting(36, 100, new LenderDto() { Available = 500, Rate = 0.01}, new LenderDto() { Available = 360, Rate = 0.02 });

            //when
            LoanDto result = _sut.Create(loanOptions);

            //then
            _mockCalculator.Verify(x => x.GetTotalRepay(It.Is<LoanDto>(l => l.Rate == 1&& l.RequestedAmount == 100)));
            Assert.That(result.TotalRepayment, Is.EqualTo(100));
        }

        [Test]
        public void should_call_the_mock_repay_calculator_to_calculate_total_repay_for_multiple_lenders()
        {
            //given
            _mockCalculator.Setup(x => x.GetTotalRepay(It.IsAny<LoanDto>())).Returns(100);
            _mockCalculator.Setup(x=>x.GetRate(It.IsAny<LoanDto>())).Returns(1.5);
            LoanOptions loanOptions = GetLoanOptionsForTesting(36, 360, new LenderDto() { Available = 180, Rate = 0.01 },
                new LenderDto() { Available = 180, Rate = 0.02 });

            //when
            LoanDto result = _sut.Create(loanOptions);

            //then
            _mockCalculator.Verify(x => x.GetTotalRepay(It.Is<LoanDto>(l => l.Rate == 1.5 && l.RequestedAmount == 360)));
            Assert.That(result.TotalRepayment, Is.EqualTo(100));
        }

        [Test]
        public void should_return_the_right_amount_of_total_repayment_for_multiple_lenders_excluding_redundant_lenders()
        {
            //given
            _mockCalculator.Setup(x => x.GetTotalRepay(It.IsAny<LoanDto>())).Returns(100);
            _mockCalculator.Setup(x => x.GetRate(It.IsAny<LoanDto>())).Returns(1);
            LoanOptions loanOptions = GetLoanOptionsForTesting(36, 1000, new LenderDto() { Available = 500 },
                new LenderDto() { Available = 2000 });

            //when
            LoanDto result = _sut.Create(loanOptions);

            //then
            _mockCalculator.Verify(x=>x.GetTotalRepay(It.Is<LoanDto>(l => l.Rate == 1 && l.RequestedAmount == 1000)));
            Assert.That(result.TotalRepayment, Is.EqualTo(100));
        }

        [Test]
        public void should_properly_populate_the_monthly_repayment_rate()
        {
            //given
            _mockCalculator.Setup(x => x.GetRate(It.IsAny<LoanDto>())).Returns(1.5);
            LoanOptions loanOptions = GetLoanOptionsForTesting(36, 360, new LenderDto() { Available = 500 },
                new LenderDto() { Available = 2000 });

            //when
            LoanDto result = _sut.Create(loanOptions);

            //then
            Assert.That(result.Rate, Is.EqualTo(1.5));
        }

        
        [Test]
        public void should_properly_populate_the_monthly_repayment()
        {
            //given
            _mockCalculator.Setup(x => x.GetMonthlyPayment(It.IsAny<LoanDto>())).Returns(10);
            _mockCalculator.Setup(x => x.GetRate(It.IsAny<LoanDto>())).Returns(1);
            LoanOptions loanOptions = GetLoanOptionsForTesting(36, 1000, new LenderDto() { Available = 500 },
                new LenderDto() { Available = 2000 });

            //when
            LoanDto result = _sut.Create(loanOptions);

            //then
            _mockCalculator.Verify(x=>x.GetMonthlyPayment(It.Is<LoanDto>(l=>l.Rate == 1 && l.RequestedAmount == 1000)));
            Assert.That(result.MonthlyRepayment, Is.EqualTo(10));
        }

        [Test]
        public void should_order_lenders_by_lowest_rate_first()
        {
            //given
            _mockCalculator.Setup(x => x.GetRate(It.IsAny<LoanDto>())).Returns(1.5);
            LoanOptions loanOptions = GetLoanOptionsForTesting(36, 200,
                new LenderDto() { Available = 100, Rate = 0.01 },
                new LenderDto() { Available = 100, Rate = 0.02 },
                new LenderDto() { Available = 100, Rate = 0.03 }
            );
            

            //when
            LoanDto result = _sut.Create(loanOptions);

            //then
            _mockCalculator.Verify(x=>x.GetRate(It.Is<LoanDto>(l => l.LendersToOfferLoan.First().Rate == 0.01 &&
            l.LendersToOfferLoan.Skip(1).First().Rate == 0.02)));
            Assert.That(result.Rate, Is.EqualTo(1.5));
        }

        [Test]
        public void should_populate_the_dto_requested_amount_properly()
        {
            //given
            LoanOptions loanOptions = GetLoanOptionsForTesting(36, 200,
                new LenderDto() { Available = 200, Rate = 300 }
            );

            //when
            LoanDto result = _sut.Create(loanOptions);

            //then
            Assert.That(result.RequestedAmount, Is.EqualTo(200));
        }

        [Test]
        public void should_populate_the_dto_monthrepaymentduration_properly()
        {
            //given
            LoanOptions loanOptions = GetLoanOptionsForTesting(36, 200,
                new LenderDto() { Available = 200, Rate = 300 }
            );

            //when
            LoanDto result = _sut.Create(loanOptions);

            //then
            Assert.That(result.DurationOfPaymentInMonths, Is.EqualTo(36));
        }

        [Test]
        public void should_mark_a_loan_as_available_if_there_is_enough_lender_amount_to_satisfy_it()
        {
            //given
            LoanOptions loanOptions = GetLoanOptionsForTesting(36, 200,
                new LenderDto() { Available = 200, Rate = 300 }
            );

            //when
            LoanDto result = _sut.Create(loanOptions);

            //then
            Assert.That(result.IsAvailable, Is.EqualTo(true));
        }
    }
}
