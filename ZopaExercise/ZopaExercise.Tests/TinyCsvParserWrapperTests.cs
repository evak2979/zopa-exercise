﻿using System.IO;
using System.Text;
using SystemWrapper.IO;
using Moq;
using NUnit.Framework;
using ZopaExercise.Business.Concretes;
using ZopaExercise.Domain.CSV;

namespace ZopaExercise.Unit
{
    [TestFixture]
    public class TinyCsvParserWrapperTests
    {
        private TinyCsvParserWrapper _sut;
        private Mock<IFileWrap> _mockFileWrapper;

        [SetUp]
        public void Setup()
        {
            _mockFileWrapper = new Mock<IFileWrap>();
            _sut = new TinyCsvParserWrapper(_mockFileWrapper.Object, new CsvFileOptions("a", Encoding.ASCII, new [] {'a'}));
        }

        [Test]
        public void should_throw_filenotfoundexception_if_csvfile_does_not_exist()
        {
            //given
            _mockFileWrapper.Setup(x => x.Exists("a")).Returns(false);
            
            //then
            Assert.Throws<FileNotFoundException>(() => _sut.Get());
        }
    }
}
