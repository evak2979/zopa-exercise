﻿using NUnit.Framework;
using ZopaExercise.Business.Concretes;

namespace ZopaExercise.Unit
{
    [TestFixture]
    public class InputValidatorTests
    {
        private InputValidator _sut;

        [SetUp]
        public void Setup()
        {
            int minAmount = 1000;
            int maxAmount = 15000;
            _sut = new InputValidator(minAmount, maxAmount);
        }

        [TestCase(999)]
        [TestCase(15001)]
        public void should_return_error_string_if_min_amount_is_less_than_or_higher_than_the_allowed_range(int requestedAmount)
        {
            //when
            string result = _sut.IsValidAmount(requestedAmount);

            //then
            Assert.That(result, Is.EqualTo("Requested amount must be between 100 and 15000"));
        }

        [Test]
        public void should_return_error_string_if_amount_is_not_divisible_by_a_hundred()
        {
            //when
            string result = _sut.IsValidAmount(1001);

            //then
            Assert.That(result, Is.EqualTo("Requested amount must be directly divisible by 100"));
        }

        [TestCase(1000)]
        [TestCase(15000)]
        [TestCase(5500)]
        public void should_return_empty_string_for_valid_amounts(int amount)
        {
            //when
            string result = _sut.IsValidAmount(amount);

            //then
            Assert.That(result, Is.EqualTo(string.Empty));
        }
    }
}
