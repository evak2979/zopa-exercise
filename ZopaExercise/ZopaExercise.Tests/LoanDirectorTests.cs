﻿using Moq;
using NUnit.Framework;
using ZopaExercise.Business.Concretes;
using ZopaExercise.Business.Interfaces;

namespace ZopaExercise.Unit
{
    [TestFixture]
    public class LoanDirectorTests
    {
        private Mock<ILoanBuilder> _mockBuilder;
        private LoanDirector _sut;

        [SetUp]
        public void Setup()
        {
            _mockBuilder = new Mock<ILoanBuilder>();
            _sut = new LoanDirector(_mockBuilder.Object);
        }

        [Test]
        public void should_call_the_initializelenders_method()
        {
            //when
            _sut.Construct();

            //then
            _mockBuilder.Verify(x=>x.InitializeLenders());
        }

        [Test]
        public void should_call_the_build_method()
        {
            //when
            _sut.Construct();

            //then
            _mockBuilder.Verify(x => x.Build());
        }
    }
}
