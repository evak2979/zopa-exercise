﻿using System.Text;
using NUnit.Framework;
using ZopaExercise.Domain.CSV;

namespace ZopaExercise.Unit
{
    [TestFixture]
    public class CsvFileOptionsTests
    {
        private CsvFileOptions _sut;

        [Test]
        public void should_initialize_csvfileoptions_properly()
        {
            //given + when
            _sut = new CsvFileOptions("A", Encoding.ASCII, new [] {'b'}, false);

            //then
            Assert.That(_sut.FileName, Is.EqualTo("A"));
            Assert.That(_sut.Encoding, Is.EqualTo(Encoding.ASCII));
            Assert.That(_sut.FieldsSeparator, Is.EqualTo(new[] {'b'}));
            Assert.That(_sut.SkipHeader, Is.EqualTo(false));
        }
    }
}
