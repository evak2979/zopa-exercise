﻿using System.Collections.Generic;

namespace ZopaExercise.Domain.DTOs
{
    public class LoanDto
    {
        public IEnumerable<LenderDto> LendersToOfferLoan { get; set; }
        public int DurationOfPaymentInMonths { get; set; }
        public double RequestedAmount { get; set; }
        public double Rate { get; set; }
        public double MonthlyRepayment { get; set; }
        public double TotalRepayment { get; set; }
        public bool IsAvailable { get; set; }
    }
}
