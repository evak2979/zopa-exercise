﻿namespace ZopaExercise.Domain.DTOs
{
    public class LenderDto
    {
        public string Name { get; set; }
        public double Rate { get; set; }
        public double Available { get; set; }
    }
}
