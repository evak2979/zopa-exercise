using System.Text;

namespace ZopaExercise.Domain.CSV
{
    public class CsvFileOptions
    {
        public string FileName { get; set; }
        public Encoding Encoding { get; set; }
        public bool SkipHeader { get; set; }
        public char[] FieldsSeparator { get; set; }

        public CsvFileOptions(string fileName, Encoding encoding, char[] fieldsSeparator, bool skipHeader = true)
        {
            FileName = fileName;
            Encoding = encoding;
            SkipHeader = skipHeader;
            FieldsSeparator = fieldsSeparator;
        }
    }
}